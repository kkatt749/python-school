# README #
  
ШЮП Артезио  
  
### What is this repository for? ###
  
Задания Python для школы  
  
### How do I get set up? ###
  
---  
  
### Contribution guidelines ###
  
Сделать форк этого репозитория (как это сделать - https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html).  
В свой репозиторий запушить последнюю версию проекта с ветки `master`.  
Не нужно заморачиваться с merge/rebase - просто скопируйте последнюю версию проекта в этот репозиторий и закомитте.  
На сайте bitbucket.org зайти в своём репозитории в раздел "Pull Request" и создать pull request в этот репозиторий (alexandr_ignatov/python-scool).  
  
Title - FirstName LastName <Email>  
Description:  
Описание изменений (E.g. Exercises for the Lesson 1)  
дополнительные сведения вроде пояснений и т.п.
  
закрывать `master` не нужно.  
Написать в Skype в чат ШЮП, что pull request готов к проверке (со ссылкой на него).  
  
### Who do I talk to? ###
  
Если какие-то вопросы, направляйте их в скайп в чат ШЮП или мне лично (live:alexandr.ignatov)  